#!/usr/bin/python


class DynamicLoader(object):
    moduleName = "log2chart"

    def makeAbsoluteModuleName(self, *args):
        return '.'.join((self.moduleName,) + args)

    def loadSymbols(self, module, symbols):
        return __import__(module,
                globals(),
                locals(),
                symbols, -1)

