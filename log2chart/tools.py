#!/usr/bin/python2

import datetime
import functools
import numbers
import types


def reverseArguments(f):
    """
    Decorator reversing the argument list of a function.
    """
    @functools.wraps(f)
    def wrapper(*args):
        return f(*reversed(args))
    return wrapper


"""
Small helper to check if type is a dictionary
"""
isDict = functools.partial(
        reverseArguments(isinstance), dict)


"""
Helper to check if type is integral
"""
isIntegral = functools.partial(
        reverseArguments(isinstance), numbers.Integral)


"""
Small helper to check if type is a datetime
"""
isDatetime = functools.partial(
        reverseArguments(isinstance), datetime.datetime)


def listImports(logger, globs = globals()):
    """
    Small utility to list all the imported modules in current scope
    """
    for v in globs.values():
        if isinstance(v, types.ModuleType):
            logger.debug("Loaded: [%s]" + v.__name__)

