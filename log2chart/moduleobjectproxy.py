#!/usr/bin/python

from log2chart.registeredobjectproxy import RegisteredObjectProxy


class ModuleObjectProxy(RegisteredObjectProxy):
    def __init__(self, package, subpackage, module, registry, *args, **kwargs):
        self.package = package
        self.subPackage = subpackage
        self.module = module

        modName = self.makeAbsoluteModuleName(self.package,
                self.subPackage, self.module)

        self.logger.debug("Building proxy for module: [%s]"
                % modName)

        super(ModuleObjectProxy, self).__init__(registry,
                modName, *args, **kwargs)

