#!/usr/bin/python

from log2chart.loggingobject import LoggingObject

from abc import ABCMeta, abstractmethod


class AbstractLogEntry(LoggingObject):
    """
    Stores Log Entry in parsed form
    """

    __metaclass__ = ABCMeta

    @abstractmethod
    def getDateTime(self):
        raise NotImplemented("Calling an interface is forbidden")

    @abstractmethod
    def getLogLevel(self):
        raise NotImplemented("Calling an interface is forbidden")

    @abstractmethod
    def getThreadId(self):
        raise NotImplemented("Calling an interface is forbidden")

    @abstractmethod
    def getSourceFile(self):
        raise NotImplemented("Calling an interface is forbidden")

    @abstractmethod
    def getLineNumber(self):
        raise NotImplemented("Calling an interface is forbidden")

    @abstractmethod
    def getLogMessage(self):
        raise NotImplemented("Calling an interface is forbidden")

    @abstractmethod
    def getProcess(self):
        raise NotImplemented("Calling an interface is forbidden")

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

