#!/usr/bin/python

from log2chart.dynamicloader import DynamicLoader
from log2chart.loggingobject import LoggingObject


class RegisteredObjectProxy(DynamicLoader, LoggingObject):

    def __init__(self, registry, module, *args, **kwargs):
        self.logger.debug("Loading module: [%s]" % module)
        factoryMod = self.loadSymbols(module, [])
        self.object = registry.registry[module](*args, **kwargs)

    def __getattr__(self, name):
        self.logger.debug("Request for attribute: [%s]" % name)
        return getattr(self.object, name)

