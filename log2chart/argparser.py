#!/usr/bin/python

from log2chart.loggingobject import LoggingObject

import argparse
from abc import ABCMeta, abstractmethod


class ArgParser(LoggingObject):
    __metaclass__ = ABCMeta

    def parse(self, *args, **kwargs):
        """
        Empty implementation by default
        """
        pass

    @abstractmethod
    def getArgParser(self):
        raise NotImplemented("Calling interface is forbidden")


class CompositeParser(ArgParser):
    __metaclass__ = ABCMeta

    @abstractmethod
    def addParser(self, parser):
        raise NotImplemented("Calling interface is forbidden")


class CompositeArgParser(CompositeParser):
    __metaclass__ = ABCMeta

    def parse(self):
        return self.parser.parse_args()

    def addParser(self, parser):
        self.parser = argparse.ArgumentParser(add_help = False,
                parents = [parser.getArgParser(), self.parser])

    def getArgParser(self):
        return self.parser
