#!/usr/bin/python


from .abstractlogentry import AbstractLogEntry
from .tools import isIntegral, isDatetime

import datetime


class LogEntry(AbstractLogEntry):

    def __init__(self, **kwargs):
        self.datetime = kwargs.get('datetime', datetime.datetime.now())
        self.loglevel = kwargs.get('loglevel', 'INFO')
        self.loggerName = kwargs.get('logger', 'default')
        self.threadid = kwargs.get('threadid', 0)
        self.process = kwargs.get('process', '')
        self.msg = kwargs.get('msg', '')
        self.sourcefile, self.linenumber = kwargs.get(
                'location', ('', 0))
        self._validate()

    def _validate(self):
        if not all((isIntegral(self.threadid),
                isIntegral(self.linenumber),
                isDatetime(self.datetime))):
            raise RuntimeError("Invalid data")

    def getDateTime(self):
        return self.datetime

    def getLogLevel(self):
        self.logger.debug("loglevel: {}".format(self.loglevel))
        return self.loglevel

    def getLogger(self):
        self.logger.debug("logger: {}".format(self.loggerName))
        return self.loggerName

    def getThreadId(self):
        self.logger.debug("threadid: {}".format(self.threadid))
        return self.threadid

    def getSourceFile(self):
        self.logger.debug("sourcefile: {}".format(self.sourcefile))
        return self.sourcefile

    def getLineNumber(self):
        self.logger.debug("linenumber: {}".format(self.linenumber))
        return self.linenumber

    def getProcess(self):
        self.logger.debug("process: %s" % self.process)
        return self.process

    def getLogMessage(self):
        self.logger.debug("msg: %s" % self.msg)
        return self.msg
