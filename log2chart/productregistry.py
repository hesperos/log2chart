#!/usr/bin/python2

class ProductRegistryMeta(type):
    def __init__(cls, name, base, dct):
        if 'registry' not in dct:
            raise TypeError("[%s] must contain 'registry' dictionary" % cls.__name__)
        super(ProductRegistryMeta, cls).__init__(name, base, dct)


class ProductRegistry(object):
    @classmethod
    def register(cls, decoratedCls):
        cls.registry[decoratedCls.__module__] = decoratedCls

    def __call__(self, cls):
        self.register(cls)
        return cls
