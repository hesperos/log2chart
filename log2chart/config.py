#!/usr/bin/python

import os
import re


class Config(object):

    def __init__(self, configPath):
        self.configPath = configPath
        self.config = {}

        self._initialise()
        self._parse()

    def _initialise(self):
        dirname = os.path.dirname(self.configPath)
        if not os.path.exists(dirname):
            os.makedirs(dirname)

    def _parse(self):
        mode = "r+" if os.path.exists(self.configPath) else "w+"
        with open(self.configPath, mode) as cFile:
            for entry in cFile:
                # skip comments
                if re.match(r'^\s*#', entry):
                    continue

                # skip invalid entries
                if len(entry) < 2 or ':' not in entry:
                    continue

                key, value = map(lambda x: x.strip(), entry.split(":"))
                self.config[key] = value

    def __getitem__(self, key):
        """
        Get the key's value or raise KeyError
        """
        return self.config[key]

    def __setitem__(self, key, value):
        """
        Set the key's value
        """
        self.config[key] = value

    def flush(self):
        """
        Flush the configuration to the config file
        """
        with open(self.configPath, "w+") as cFile:
            for etuple in self.config.items():
                entry = ':'.join(etuple) + "\n"
                cFile.write(entry)


class ConfigSetup(object):
    defaultHome = os.environ['HOME'] if 'HOME' in os.environ else '~'
    confDir = "log2chart"

    def __init__(self,
            configFile = 'log2chart.conf',
            defaultSettings = {}):

        self.defaultSettings = defaultSettings

        self.configPath = os.path.join(self.defaultHome,
                ".config",
                self.confDir,
                configFile)

        if 'XDG_CONFIG_HOME' in os.environ:
            self.configPath = os.path.join(os.environ['XDG_CONFIG_HOME'],
                    self.confDir,
                    configFile)

    def getConfig(self):
        config = Config(self.configPath)

        for k,v in self.defaultSettings.items():
            try:
                config[k]
            except KeyError:
                config[k] = v

        config.flush()
        return config
