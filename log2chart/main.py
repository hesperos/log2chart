#!/usr/bin/python

from log2chart import config, mainparser, logger, tools
import log2chart.renderer.factory as lrf
import log2chart.parser.factory as lpf

def main():
    # get main configuration
    cfg = config.ConfigSetup('log2chart.conf', {
        'parser': 'regex',
        'verbosity': '1',
                }).getConfig()

    # early initialize the logger
    log = logger.Logger()
    log.setVerbosity(int(cfg['verbosity']))

    # prepare main parser
    argParser = mainparser.MainParser()

    # make the factories
    parserFactory = lpf.ParserFactory(cfg['parser'])
    rendererFactory = lrf.RendererFactory()

    # setup parsers
    argParser.addParser(parserFactory.makeArgParser())
    argParser.addParser(rendererFactory.makeArgParser())

    # parse arguments
    opts = argParser.parse()

    # adjust verbosity
    log.setVerbosity(opts.verbose)
    log.get().debug("Command: [%s]" % opts.command)
    log.get().debug("Verbosity: [%d]" % opts.verbose)

    try:
        parser = parserFactory.makeParser(opts.logfile, opts)
        renderer = rendererFactory.makeRenderer(parser, opts.command)
    except ImportError as e:
        log.get().critical("Failed loading parser/renderer: %s" % str(e))
        return -1

    try:
        # display some debugging info about loaded modules
        tools.listImports(log.get(), globals())

        # let the renderer take over
        renderer.render(opts)
    except:
        log.get().critical("Failed running renderer: %s" % str(e))
        return -1

    # we are done
    return 0
