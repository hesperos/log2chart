#!/usr/bin/python

from .logentry import LogEntry

import re


class FunctionLogEntry(LogEntry):
    ENTER = 0
    EXIT = 1
    types = { 'ENTER': ENTER, 'EXIT': EXIT }

    def __init__(self, **kwargs):
        super(FunctionLogEntry, self).__init__(**kwargs)
        self.type, self.signature = self._parseMessage()

    def _parseMessage(self):
        msg = self.getLogMessage()
        possibleKeys = r'|'.join(self.types.keys())

        tokenized = re.split(r':\s*', msg, 1)
        try:
            t = self.types[ tokenized[0] ]
            s = tokenized[1]
        except (KeyError, IndexError):
            raise RuntimeError("Doesn't appear to be a function log entry")
        else:
            return t,s

    def getType(self):
        self.logger.debug("type: [%d]" % self.type)
        return self.type

    def getSignature(self):
        self.logger.debug("signature: [%s]" % self.signature)
        return self.signature
