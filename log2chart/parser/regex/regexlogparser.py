#!/usr/bin/python

from log2chart.parser.abstractlogparser import AbstractLogParser
from log2chart.logentry import LogEntry
from log2chart.functionlogentry import FunctionLogEntry
from log2chart.parser.registry import ParserRegistry, ParserArgParserRegistry

from . import propertyfile, fakepropertyfile
from .patternconverterfactory import PatternConverterFactory

from collections import OrderedDict
import re


@ParserRegistry()
class InterfaceAdapter(AbstractLogParser):

    def __init__(self, logfile, opts):
        self.parser = RegexLogParser.fromOpts(opts, logfile)

    def parseLine(self):
        return self.parser.parseLine()


class RegexLogParser(AbstractLogParser):
    """
    Log parser utilizing regular expressions
    """

    @classmethod
    def fromPropertyFile(cls, propFile, appender, logfile):
        try:
            propertyTree = propFile.getPropertyTree()['log4cplus']
        except KeyError:
            self.logger.critical("Invalid format of property file")
            raise

        cp = propertyTree['appender'][appender]['layout']['ConversionPattern']
        return cls(cp, logfile)

    @classmethod
    def fromOpts(cls, opts, logFile):
        try:
            pf = (propertyfile.PropertyFile(opts.property_file)
                if opts.property_file else fakepropertyfile.FakePropertyFile())

            appender = opts.appender if opts.appender else 'logfile';

            parser = cls.fromPropertyFile(pf,
                appender, logFile)

        except RuntimeError as e:
            self.logger.critical("Error loading property file: " + str(e))
            raise

        else:
            return parser

    def __init__(self, conversionPattern, logfile):
        """
        Simple ctor
        """
        self.cp = conversionPattern
        self.f = logfile
        self.pcs = self._parseConversionPattern()

        self.logger.debug("Using cp [%s]" % conversionPattern)

    @staticmethod
    def _tokenizeConversionPattern(cp):
        """
        Returns a list of tokens out of given conversion pattern
        """
        r = re.compile(r'\%[-0-9\.]*\w+(?:\{[^\{\}]+\})*')
        return re.findall(r, cp)

    def _parseConversionPattern(self):
        """
        Parses conversion pattern and creates pattern converters
        """
        pcf = PatternConverterFactory()
        converters = OrderedDict()
        pcIgnore = [ 'ndc' ]
        for t in self._tokenizeConversionPattern(self.cp):
            pc = pcf.getPatternConverter(t)
            if pc.kwargKey in pcIgnore:
                continue
            converters[pc.kwargKey] = pc
        return converters

    def _createKwargs(self, match):
        """
        Creates kwargs for pattern converters
        """
        kwargs = {}
        for pc,m in zip(self.pcs.values(), match.groups()):
            self.logger.debug("pc: {}, match: {}".format(pc,m))
            kwargs[pc.kwargKey] = pc.processMatch(m)
        return kwargs

    def _createLogEntry(self, match):
        """
        Creates either LogEntry or FunctionLogEntry
        """
        kwargs = self._createKwargs(match)
        try:
            fe = FunctionLogEntry(**kwargs)
        except RuntimeError:
            return LogEntry(**kwargs)
        else:
            return fe

    def getRegex(self):
        """
        Creates a line parsing regex
        """
        r = r'\s*'.join(map(lambda pc: pc.regex, self.pcs.values()))
        return re.compile(r)

    def parseLine(self):
        """
        Parses a single log line entry
        """
        for line in iter(self.f.readline, b''):
            match = re.search(self.getRegex(), line)
            if match:
                yield self._createLogEntry(match)
            else:
                self.logger.warning("IGNORED! Line doesn't match [%s]" %
                        line.replace('\n', '\\n'))
