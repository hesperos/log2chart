#!/usr/bin/python

from log2chart.parser.regex.abstractpropertyfile import AbstractPropertyFile

class FakePropertyFile(AbstractPropertyFile):

    def getPropertyTree(self):
        propertyTree = {'log4cplus':
                { 'appender': { 'logfile': {
                    'layout': {
                        'ConversionPattern': '%D{%Y-%m-%d %H:%M:%S,%q} %-5p %x %t %c %l %m%n',
                        'layout': 'log4cplus::PatternLayout',
                        }
                    }
                    }
                    }
                }

        return propertyTree

