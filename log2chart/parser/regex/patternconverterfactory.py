#!/usr/bin/python

import re

from log2chart.parser.regex import patternconverters as pcs
from log2chart.loggingobject import LoggingObject

class PatternConverterFactory(LoggingObject):
    """
    This interface is used by regex log parser to provide a factory to build
    pattern converters out of conversion pattern tokens.
    """

    def __init__(self):
        pass

    def getPatternConverter(self, pattern):
        pattern.strip()
        if not pattern.startswith('%'):
            raise RuntimeError("Token doesn't describe pattern")

        # log4cplus not implemented M, r, X
        pcLookup = {
                'b': pcs.BaseNamePatternConverter,
                'c': pcs.LoggerPatternConverter,
                'd': pcs.DatePatternConverter,
                'D': pcs.DatePatternConverter,
                'F': pcs.FilePatternConverter,
                'h': pcs.HostNamePatternConverter,
                'H': pcs.HostNamePatternConverter,
                'i': pcs.ProcessPatternConverter,
                'l': pcs.LocationPatternConverter,
                'L': pcs.LinePatternConverter,
                'm': pcs.MsgPatternConverter,
                'n': pcs.NewLinePatternConverter,
                'p': pcs.LogLevelPatternConverter,
                't': pcs.ThreadPatternConverter,
                'x': pcs.NDCPatternConverter,
                }

        rm = re.search(r'[a-zA-Z]+', pattern)

        if not rm:
            raise NotImplementedError("Incorrect pattern")

        try:
            self.logger.debug("Looking up converter for pattern: %s" % rm.group(0))
            pc = pcLookup[rm.group(0)](pattern)
        except KeyError as e:
            raise NotImplementedError(
                    "Incorrect pattern or not implemented: " + str(e))
        else:
            self.logger.debug("Returning pc")
            return pc
