#!/usr/bin/python


from log2chart.parser.regex.abstractpropertyfile import AbstractPropertyFile
from log2chart.tools import isDict

import re


class PropertyFile(AbstractPropertyFile):

    def __init__(self, pf):
        self.pf = pf
        self.propertyTree = self._parse(self.pf)


    def _convertPathToDict(self, l):
        """
        Small helper converting the object path definition
        to python nested dictionary structure.
        """

        if '=' not in l:
            raise RuntimeError("Line: [" +
                    l.strip('\n') +
                    "], doesn't contain any property definition")

        objpath, value = map(lambda x: x.strip(), l.split('=', 1) )

        def x(o, i, v):
            try:
                k = o[i]
            except IndexError:
                return v
            else:
                return { k: x(o, i + 1, v) }

        return x(objpath.split('.'), 0, value)


    def _mergeDictionaries(self, a,b):
        """
        Recursively merge b into a. This function mutates a.

        Algorithm:
        1. Key doesn't exist in a -> insert it.
        2. Key exists in a:
            - Key value is a dictionary
                - b's value is a dictionary -> recursive call
                - b's value is not a dictionary -> create dict(), recursive call

            - Key value is not a dictionary -> create dict(), recursive call
        """
        for k, v in b.items():
            if k in a:
                if isDict(a[k]):
                    if (isDict(v)):
                        self._mergeDictionaries(a[k], v)
                    else:
                        self._mergeDictionaries(a[k], { k: v })
                else:
                    a[k] = { k: a[k] }
                    self._mergeDictionaries(a[k], v)
            else:
                a[k] = v

        return a


    def _parse(self, f):
        a = {}
        f.seek(0)

        for line in f:
            line = line.strip()
            if not len(line) or re.match(r'#', line):
                continue
            b = self._convertPathToDict(line)
            a = self._mergeDictionaries(a,b)

        return a


    def getPropertyTree(self):
        """
        Returns property file in parsed dictionary format
        """
        return self.propertyTree

