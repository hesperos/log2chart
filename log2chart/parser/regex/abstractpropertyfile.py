#!/usr/bin/python

from log2chart.loggingobject import LoggingObject

from abc import ABCMeta, abstractmethod


class AbstractPropertyFile(LoggingObject):

    __metaclass__ = ABCMeta

    @abstractmethod
    def getPropertyTree(self):
        raise NotImplemented("Calling an interface is forbidden")

