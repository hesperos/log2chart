#!/usr/bin/python

import datetime
import re

from .abstractpatternconverter import AbstractPatternConverter


class DatePatternConverter(AbstractPatternConverter):
    kwargKey = 'datetime'

    def __init__(self, pattern):
        # hard code for now, since most likely the majority
        # will use the same format
        if pattern != '%D{%Y-%m-%d %H:%M:%S,%q}':
            raise NotImplementedError(
                    'Pattern support not implemented: ' + pattern)

        m = re.search("\{(.+)\}", pattern)

        if m:
            self.format = m.group(1)
            self.format = str.replace(self.format, "%q", "%f")
        else:
            self.format = '%Y-%m-%d %H:%M:%S,%f'

        self.logger.debug("date format: ".format(self.format))
        self.regex = r'(\d+-\d+-\d+\s?\d+:\d+:\d+,\d+)'

    def processMatch(self, match):
        return datetime.datetime.strptime(match, self.format)


class LocationPatternConverter(AbstractPatternConverter):
    regex = r'([-\d\w/\.]+:\d+)'
    kwargKey = 'location'

    def processMatch(self, match):
        self.logger.debug("match: {}".format(match))
        try:
            p,s = match.split(':')
            s = int(s)
        except ValueError:
            self.logger.error(
                    "Unable to parse LocationEntry. Check conversion pattern!")
            return "",0
        else:
            return p,s

class MsgPatternConverter(AbstractPatternConverter):
    regex = r'(.*)'
    kwargKey = 'msg'


class NewLinePatternConverter(AbstractPatternConverter):
    regex = r'\Z'
    kwargKey = 'newline'


class LogLevelPatternConverter(AbstractPatternConverter):
    regex = r'(\w+)'
    kwargKey = 'loglevel'


class ThreadPatternConverter(AbstractPatternConverter):
    regex = r'(\d+)'
    kwargKey = 'threadid'

    def processMatch(self, match):
        return int(match)


class NDCPatternConverter(AbstractPatternConverter):
    # not supported at the moment
    regex = r''
    kwargKey = 'ndc'


class ProcessPatternConverter(AbstractPatternConverter):
    regex = r'([\S]+)'
    kwargKey = 'process'

################################################################################


class BaseNamePatternConverter(AbstractPatternConverter):
    pass


class FilePatternConverter(AbstractPatternConverter):
    pass


class HostNamePatternConverter(AbstractPatternConverter):
    pass


class LinePatternConverter(AbstractPatternConverter):
    pass


class LoggerPatternConverter(AbstractPatternConverter):
    # the logger as configures in the property file
    regex = r'([\S^\.]+)'
    kwargKey = 'logger'
    pass
