#!/usr/bin/python

from log2chart.loggingobject import LoggingObject

class AbstractPatternConverter(LoggingObject):
    """
    This interface is used by regex log parser to provide regex extracting a
    given pattern.
    """

    # regex to extract the pattern
    regex = r''

    # key used to provide as kwarg to LogEntry
    kwargKey = ''


    def __init__(self, pattern):
        self.pattern = pattern

    def processMatch(self, match):
        return match
