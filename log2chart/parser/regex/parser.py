#!/usr/bin/python

from log2chart.argparser import ArgParser
from log2chart.parser.registry import ParserArgParserRegistry

import argparse


@ParserArgParserRegistry()
class RegexArgParser(ArgParser):
    def __init__(self):
        self.parser = argparse.ArgumentParser(add_help = False)

        self.parser.add_argument("--property-file",
                "-p",
                nargs = '?',
                type = argparse.FileType('r'),
                help = "log4cplus property file")

        self.parser.add_argument("--appender",
                "-a",
                nargs = '?',
                help = "appender used to as a conversion pattern source")

    def getParserModule(self):
        return 'regexlogparser'

    def getArgParser(self):
        return self.parser


