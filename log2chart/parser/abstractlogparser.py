#!/usr/bin/python

from log2chart.loggingobject import LoggingObject

from abc import ABCMeta, abstractmethod

class AbstractLogParser(LoggingObject):

    __metaclass__ = ABCMeta

    @abstractmethod
    def parseLine(self):
        """
        Parse a single log line and produce a log entry out of it
        """
        raise NotImplemented("Calling an interface is forbidden")
