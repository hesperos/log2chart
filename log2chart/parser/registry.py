#!/usr/bin/python

from log2chart.productregistry import ProductRegistry, ProductRegistryMeta


class ParserRegistry(ProductRegistry):
    """
    Registry used to translate parser's module -> class
    """
    registry = {}
    __metaclass__ = ProductRegistryMeta


class ParserArgParserRegistry(ProductRegistry):
    """
    Registry used to translate parser's ArgParser module -> class
    """
    registry = {}
    __metaclass__ = ProductRegistryMeta

