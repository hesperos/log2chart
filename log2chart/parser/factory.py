#!/usr/bin/python

from log2chart.loggingobject import LoggingObject
from log2chart.moduleobjectproxy import ModuleObjectProxy
from log2chart.parser.registry import ParserRegistry, ParserArgParserRegistry
from log2chart.registeredobjectproxy import RegisteredObjectProxy


class ParserObjectProxy(ModuleObjectProxy):
    def __init__(self, rendererName, module, registry, *args, **kwargs):
        super(ParserObjectProxy, self).__init__('parser',
                rendererName, module, registry, *args, **kwargs)


class ParserArgParserProxy(ParserObjectProxy):

    def __init__(self, parserName):
        super(ParserArgParserProxy, self).__init__(parserName,
                'parser', ParserArgParserRegistry)


class ParserProxy(ParserObjectProxy):

    def __init__(self, parserName, module, logfile, opts):
        super(ParserProxy, self).__init__(parserName,
                module, ParserRegistry, logfile, opts)


class ParserFactory(LoggingObject):

    def __init__(self, parserName):
        self.parserName = parserName
        self.parserModule = None

    def makeArgParser(self):
        try:
            p = ParserArgParserProxy(self.parserName)
            self.parserModule = p.getParserModule()
            return p

        except ImportError as e:
            self.logger.warning("Failure creating parser [%s] due to: %s" %
                    (self.parserName, str(e)))
            raise

        except KeyError as e:
            self.logger.warning("Skipping [%s], most likely not registered: %s" %
                    (r, str(e)))

    def makeParser(self, logfile, opts):
        try:
            return ParserProxy(self.parserName,
                    self.parserModule,
                    logfile, opts)

        except ImportError as e:
            self.logger.warning("Failure creating parser [%s] due to: %s" %
                    (self.parserName, str(e)))
            raise

        except KeyError as e:
            self.logger.warning("Skipping [%s], most likely not registered: %s" %
                    (r, str(e)))

