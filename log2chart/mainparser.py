#!/usr/bin/python

from log2chart.argparser import CompositeArgParser

import argparse
import copy
import sys


class MainParser(CompositeArgParser):
    description = "plot graphs out of log4cplus data"
    parser = argparse.ArgumentParser(prog = sys.argv[0],
            description = description)

    def __init__(self):
        self.parser.add_argument("--verbose",
                "-v",
                action = 'count',
                default = 0)

        self.parser.add_argument("logfile",
                type = argparse.FileType('r'),
                help = "logfile to process")
