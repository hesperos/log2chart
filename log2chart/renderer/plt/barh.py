from log2chart.renderer.abstractrenderer import AbstractRenderer
from log2chart.functionlogentry import FunctionLogEntry
from log2chart.renderer.tools import sieveFEntries
from log2chart.renderer.registry import RendererRegistry

from collections import defaultdict

import datetime
import operator
import matplotlib.pyplot as plt


@RendererRegistry()
class PltBarh(AbstractRenderer):

    def __init__(self, parser):
        super(PltBarh, self).__init__(parser)
        self.fentries = []
        self.entries = []

    def render(self, opts):
	self.fentries, self.entries = sieveFEntries(self.parser)

        self.logger.info("Entries/Fentries: {}/{}".format(
            len(self.entries), len(self.fentries)))

        # sort by thread first
        self.fentries.sort(key = lambda k: k.getThreadId())

        callStack = []
        maxStackDepth = 0
        stackTrace = defaultdict(list)
	fig, ax = plt.subplots()

        epoch = datetime.datetime.utcfromtimestamp(0)

        for fe in self.fentries:
            if fe.getType() == FunctionLogEntry.ENTER:
                callStack.append(fe)
                continue
            try:
                prev = callStack.pop()
            except IndexError:
                self.logger.error("CallStack broken, ignoring entry for: " + fe.getSignature())
                continue

            if prev.getSignature() != fe.getSignature():
                self.logger.error("CallStack broken, ignoring entry for: " + fe.getSignature())
                continue

            durationMs = (fe.getDateTime() - prev.getDateTime()).total_seconds()*1000.0
            enterTs = (prev.getDateTime() - epoch).total_seconds() * 1000.0
            stackTrace[len(callStack)].append((enterTs, durationMs))

            maxStackDepth = max(maxStackDepth, len(callStack))

        for k,v in sorted(stackTrace.items(),
                key = operator.itemgetter(0)):
            ax.broken_barh(v, (k, 0.95), facecolors='blue',
                    lw = 1)

        for e in self.entries:
            x = (e.getDateTime() - epoch).total_seconds() * 1000.0
            ax.plot([x, x], [0, maxStackDepth + 1], color = 'red')

	ax.set_xlabel('milliseconds')
	ax.set_ylabel('stack depth')
	ax.grid(True)
	plt.show()

