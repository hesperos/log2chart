#!/usr/bin/python

from log2chart.renderer.parser import RendererParser
from log2chart.renderer.registry import RendererParserRegistry


@RendererParserRegistry()
class Parser(RendererParser):
    def __init__(self, parser):
        super(Parser, self).__init__(parser)
        self.addCommand('barh', 'barh', help = "profiling chart with plt.barh")
