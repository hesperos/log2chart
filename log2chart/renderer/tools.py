#!/usr/bin/python

from log2chart.functionlogentry import FunctionLogEntry
from log2chart.tools import reverseArguments

import functools


isFEntry = functools.partial(
        reverseArguments(isinstance), FunctionLogEntry)


def sieveFEntries(parser):
    entries = []
    fentries = []
    for e in parser.parseLine():
        (fentries if isFEntry(e) else entries).append(e)
    return fentries, entries

