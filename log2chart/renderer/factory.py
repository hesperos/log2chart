#!/usr/bin/python

from log2chart.argparser import ArgParser
from log2chart.loggingobject import LoggingObject
from log2chart.moduleobjectproxy import ModuleObjectProxy
from log2chart.renderer.registry import RendererParserRegistry, RendererRegistry

import argparse
import pkgutil
import os


class RendererObjectProxy(ModuleObjectProxy):
    def __init__(self, rendererName, module, registry, *args, **kwargs):
        super(RendererObjectProxy, self).__init__('renderer',
                rendererName, module, registry, *args, **kwargs)


class RendererParserProxy(RendererObjectProxy):

    def __init__(self, rendererName, parser):
        super(RendererParserProxy, self).__init__(rendererName,
                'parser', RendererParserRegistry, parser)


class RendererProxy(RendererObjectProxy):

    def __init__(self, rendererName, module, parser):
        super(RendererProxy, self).__init__(rendererName,
                module, RendererRegistry, parser)


class RendererFactory(LoggingObject):

    class RendererParser(ArgParser):
        parser = argparse.ArgumentParser(add_help = False)
        subparser = parser.add_subparsers(dest = "command",
                metavar = "command")

        def getArgParser(self):
            return self.parser


    def __init__(self):
        self.root = os.path.dirname(os.path.abspath(__file__))

        # create a list of all renderer sub-packages
        self.renderers = [ name for _, name, isPkg in
                pkgutil.walk_packages(path = [self.root]) if isPkg ]

        # mapping: command -> module
        self.cmdToModule = {}

        # mapping: command -> renderer
        self.cmdToRenderer = {}

    def makeArgParser(self):
        parser = RendererFactory.RendererParser()

        for r in self.renderers:
            try:
                p = RendererParserProxy(r, parser.subparser)
                cmds = p.getCommands()

                self.cmdToModule.update(cmds)
                self.cmdToRenderer.update(dict(
                    zip(cmds.keys(), [r] * len(cmds))))

            except ImportError as e:
                self.logger.warning("Skipping [%s] due to: %s" %
                        (r, str(e)))

            except KeyError as e:
                self.logger.warning("Skipping [%s], most likely not registered: %s" %
                        (r, str(e)))

        return parser

    def makeRenderer(self, parser, cmd):

        try:
            renderer = RendererProxy(
                    self.cmdToRenderer[cmd],
                    self.cmdToModule[cmd],
                    parser)

        except ImportError as e:
            self.logger.warning("Failed [%s] due to: %s" %
                    (cmd, str(e)))
            raise

        except KeyError as e:
            self.logger.warning("Couldn't lookup data for command: [%s], or renderer not registered: %s" %
                    (cmd, str(e)))
            raise

        else:
            return renderer
