#!/usr/bin/python

from log2chart.renderer.abstractrenderer import AbstractRenderer
from log2chart.renderer.tools import sieveFEntries

import gviz_api
import datetime


class GChartRenderer(AbstractRenderer):

    page_template = """
<html>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script>
    google.charts.load('current', {'packages':['gantt']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      var jsonChart =
          new google.visualization.Gantt(
              document.getElementById('chart_div'));

      var json_data = new google.visualization.DataTable(%(json)s, 0.6);
      var options = {
        height: 400,
        gantt: {
            trackHeight: 30
        }
      };
      jsonChart.draw(json_data, options);
    }
  </script>
  <body>
    <div id="chart_div"></div>
  </body>
</html>
"""

    def __init__(self, parser):
        super(GChartRenderer, self).__init__(parser)
        self.fentries = []
        self.entries = []

    def render(self):
        raise NotImplementedError("This renderer is not implemented")

        self.fentries, self.entries = sieveFEntries(self.parser)

        description = {
                "threadStack": ("string", "Thread Stack"),
                "proto": ("string", "Prototype"),
                "enter": ("datetime", "Enter timestamp"),
                "exit": ("datetime", "Exit timestamp"),
                }

        data = [{
                "threadStack": "xxx01",
                "proto": "xxx",
                "enter": datetime.datetime.now(),
                "exit": datetime.datetime.now() + datetime.timedelta(hours = 1)
                },
                ]


        # Loading it into gviz_api.DataTable
        data_table = gviz_api.DataTable(description)
        data_table.LoadData(data)

        # Create a JSON string.
        json = data_table.ToJSon(
                columns_order=("threadStack", "proto", "enter", "exit"))
                # order_by="salary")

        print self.page_template % vars()




def makeRenderer(parser):
    return GChartRenderer(parser)
