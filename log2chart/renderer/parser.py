#!/usr/bin/python

from log2chart.argparser import ArgParser

class RendererParser(ArgParser):

    def __init__(self, parser):
        self.parser = parser
        self.cmds = {}

    def addCommand(self, cmd, module, **kwargs):
        self.cmds[cmd] = (self.parser.add_parser(cmd, **kwargs),
                module)

    def getCommands(self):
        """
        Returns a mapping: command -> module, for all commands
        """
        return { cmd: entry[1] for cmd, entry in self.cmds.items() }

    def getCmdParser(self, cmd):
        return self.cmds[cmd][0]

    def getArgParser(self):
        return self.parser


