#!/usr/bin/python

from log2chart.renderer.parser import RendererParser
from log2chart.renderer.registry import RendererParserRegistry


@RendererParserRegistry()
class ProfilerParser(RendererParser):

    def __init__(self, parser):
        super(ProfilerParser, self).__init__(parser)
        self.addCommand('simplep', 'simple', help = "simple profiler")
        self.getCmdParser('simplep').add_argument("--sort",
                "-s",
                default = 'count',
                choices = [ 'count', 'time' ],
                help = 'sort profile output by')

        self.getCmdParser('simplep').add_argument("--group-events",
                "-g",
                default = 'source',
                choices = [ 'source', 'thread', 'loglevel', 'process' ])
