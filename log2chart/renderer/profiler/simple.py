#!/usr/bin/python

from log2chart.renderer.abstractrenderer import AbstractRenderer
from log2chart.renderer.tools import sieveFEntries
from log2chart.renderer.registry import RendererRegistry
from log2chart.functionlogentry import FunctionLogEntry
from log2chart.logentry import LogEntry

from collections import defaultdict
import datetime


@RendererRegistry()
class SimpleProfiler(AbstractRenderer):

    def __init__(self, parser):
        super(SimpleProfiler, self).__init__(parser)
        self.fentries = []
        self.entries = []

    def render(self, opts):
        self.fentries, self.entries = sieveFEntries(self.parser)

        self.logger.info("Entries/Fentries: {}/{}".format(
            len(self.entries), len(self.fentries)))

        # sort by thread first
        self.fentries.sort(key = lambda k: k.getThreadId())

        callStack = []
        funcSummary = defaultdict(list)
        eventSummary = defaultdict(list)

        for fe in self.fentries:
            if fe.getType() == FunctionLogEntry.ENTER:
                callStack.append(fe)
                continue
            try:
                prev = callStack.pop()
            except IndexError:
                self.logger.error("CallStack broken, ignoring entry for: " + fe.getSignature())
                continue

            if prev.getSignature() != fe.getSignature():
                self.logger.error("CallStack broken, ignoring entry for: " + fe.getSignature())
                continue

            funcSummary[fe.getSignature()].append(
                    fe.getDateTime() - prev.getDateTime())

        groupingFunc = {
                'source': LogEntry.getSourceFile,
                'thread': LogEntry.getThreadId,
                'loglevel': LogEntry.getLogLevel,
                'process': LogEntry.getProcess,
                }

        for e in self.entries:
            eventSummary[groupingFunc[opts.group_events](e)].append(e)

        sortedKey = ((lambda i: len(i[1]))
                if opts.sort == 'count' else
                (lambda i: sum(i[1], datetime.timedelta())))

        for k,v in sorted(funcSummary.items(),
                key = sortedKey, reverse = True):
            print "{0:10}, {1:24}, {2}".format(len(v),
                    sum(v, datetime.timedelta()), k)

        print "\nEvent Summary (grouped by %s):\n" % opts.group_events

        for k,v in sorted(eventSummary.items(),
                key = lambda e: len(e[1]), reverse = True):
            print "{0:10}, {1}".format(len(v), k)
