#!/usr/bin/python

from log2chart.productregistry import ProductRegistry, ProductRegistryMeta


class RendererRegistry(ProductRegistry):
    """
    Renderer module -> Renderer class
    """
    registry = {}
    __metaclass__ = ProductRegistryMeta


class RendererParserRegistry(ProductRegistry):
    """
    Renderer parser module -> Renderer class
    """
    registry = {}
    __metaclass__ = ProductRegistryMeta

