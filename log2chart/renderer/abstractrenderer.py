#!/usr/bin/python

from log2chart.loggingobject import LoggingObject

from abc import ABCMeta, abstractmethod


class AbstractRenderer(LoggingObject):

    __metaclass__ = ABCMeta

    def __init__(self, parser):
        self.parser = parser

    @abstractmethod
    def render(self, opts):
        raise NotImplemented("Calling an interface is forbidden")

