#!/usr/bin/python

import logging
import sys


class Logger(object):
    logFormat = ('%(asctime)s %(levelname)s %(funcName)s:%(lineno)d %(message)s')

    def __init__(self):
        self.formatter = logging.Formatter(self.logFormat)
        self.logger = logging.getLogger()
        self.handler = logging.StreamHandler(sys.stderr)
        self.handler.setFormatter(self.formatter)

        self.setVerbosity(0)

        self.logger.addHandler(self.handler)

    @staticmethod
    def _verbosityToLevel(verbosity):
        logLevels = [ logging.ERROR,
                logging.WARNING,
                logging.INFO,
                logging.DEBUG ]

        try:
            logLevel = logLevels[ verbosity ]
        except IndexError:
            return logLevels[-1]
        else:
            return logLevel

    def _setLevel(self, logLevel):
        self.logger.setLevel(logLevel)
        self.handler.setLevel(logLevel)

    def setVerbosity(self, verbosity):
        logLevel = self._verbosityToLevel(verbosity)
        self._setLevel(logLevel)

    def get(self):
        return self.logger
