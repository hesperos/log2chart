#!/usr/bin/python

import unittest
import __builtin__
import functools

from log2chart.dynamicloader import DynamicLoader

class FakeImport(object):
    def __init__(self):
        self.args = []
        self.kwargs = {}
        self.counter = 0

    def fakeImport(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.counter += 1

class TestDynamicLoader(unittest.TestCase):

    def setUp(self):
        self.fakeImport = FakeImport()
        self.originalImport = __builtin__.__import__
        __builtin__.__import__ = functools.partial(self.fakeImport.fakeImport,
                self.fakeImport)

    def tearDown(self):
        __builtin__.__import__ = self.originalImport

    def test_ifMakeAbsoluteModuleNameReturnsCorrectModuleNames(self):
        d = DynamicLoader()
        self.assertEqual(d.makeAbsoluteModuleName('some', 'package', 'submodule'),
                "log2chart.some.package.submodule")

    def test_ifDynamicLoaderCallsLoadSymbols(self):
        d = DynamicLoader()
        module = 'some.test.module'
        symbols = [ 'symbol1', 'symbol2' ]

        d.loadSymbols(module, symbols)

        self.assertEqual(self.fakeImport.counter, 1)
        self.assertEqual(self.fakeImport.args[1], module)
        self.assertEqual(self.fakeImport.args[4], symbols)


if __name__ == '__main__':
    unittest.main()

