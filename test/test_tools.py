#!/usr/bin/python

import unittest
import datetime

from log2chart import tools

class TestTools(unittest.TestCase):

    def setUp(self):
        pass

    def test_reverseArguments(self):

        @tools.reverseArguments
        def func(a,b,c):
            self.assertTrue(isinstance(a, int))
            self.assertTrue(isinstance(b, list))
            self.assertTrue(isinstance(c, dict))

        func({}, [], 123)

    def test_isDict(self):
        self.assertTrue(tools.isDict({}))
        self.assertFalse(tools.isDict([]))

    def test_isIntegral(self):
        self.assertTrue(tools.isIntegral(0))
        self.assertTrue(tools.isIntegral(123))
        self.assertFalse(tools.isIntegral('123'))
        self.assertFalse(tools.isIntegral(120.4))
        self.assertFalse(tools.isIntegral("abc"))

    def test_isDatetime(self):
        self.assertTrue(tools.isDatetime(datetime.datetime.now()))
        self.assertFalse(tools.isDatetime(123))
        self.assertFalse(tools.isDatetime('2010-11-11 10:12:33'))
        self.assertFalse(tools.isDatetime('some string'))


if __name__ == '__main__':
    unittest.main()
