#!/usr/bin/python

import StringIO
import logging
import os
import re
import unittest
import datetime

from log2chart.parser.regex import regexlogparser as rlp
from log2chart import abstractlogentry
from log2chart import logentry, functionlogentry


class TestRegexLogParser(unittest.TestCase):

    def setUp(self):
        logging.basicConfig(level = logging.ERROR)

        self.cp = '%D{%Y-%m-%d %H:%M:%S,%q} %-5p %x %t %l %m%n'
        self.regex = re.compile(r'(\d+-\d+-\d+\s?\d+:\d+:\d+,\d+)'
                r'\s*(\w+)\s*(\d+)\s*([-\d\w/\.]+:\d+)\s*(.*)\s*\Z')

        self.memFile = StringIO.StringIO()
        self.memFile.write('2015-06-16 14:32:06,530 INFO   731243808 /some/path/with.dots/file.cpp:1131 This is the message\n')
        self.memFile.write('2015-06-16 14:32:06,531 DEBUG  788342048 /other/path/other.cpp:926 ENTER: bla bla\n')
        self.memFile.seek(0)

        self.dateFormat = '%Y-%m-%d %H:%M:%S,%f'

        self.parsedEntries = []
        self.parsedEntries.append(logentry.LogEntry(
                    datetime = datetime.datetime.strptime('2015-06-16 14:32:06,530', self.dateFormat),
                    loglevel = 'INFO',
                    threadid = 731243808,
                    location = ('/some/path/with.dots/file.cpp', 1131),
                    msg = 'This is the message'))
        self.parsedEntries.append(functionlogentry.FunctionLogEntry(
                    datetime = datetime.datetime.strptime('2015-06-16 14:32:06,531', self.dateFormat),
                    loglevel = 'DEBUG',
                    threadid = 788342048,
                    location = ('/other/path/other.cpp', 926),
                    msg = 'ENTER: bla bla'))


    def test_ifProducesCorrectRegex(self):
        parser = rlp.RegexLogParser(self.cp, self.memFile)
        self.assertEqual(self.regex, parser.getRegex())


    def test_ifParsesTheTestMemFile(self):
        parser = rlp.RegexLogParser(self.cp, self.memFile)
        lastIdx = 0;

        for idx, entry in enumerate(parser.parseLine(), 0):
            lastIdx = idx
            self.assertIsInstance(entry, abstractlogentry.AbstractLogEntry)
            self.assertEqual(entry, self.parsedEntries[idx])

        self.assertEqual(2, lastIdx + 1)


class RealDataTestRegexLogParser(unittest.TestCase):

    def setUp(self):
        basePath = os.path.dirname(os.path.abspath(__file__))
        self.assetsPath = os.path.join(basePath, 'data')
        self.cps = []

        self.cps.append('%D{%Y-%m-%d %H:%M:%S,%q} %-5p %x %t %l %m%n')
        self.cps.append('%D{%Y-%m-%d %H:%M:%S,%q} %-5p %x %t %i %l %m%n')
        self.cps.append('%D{%Y-%m-%d %H:%M:%S,%q} %-5p %x %t %i %l %m%n')

    def test_ifParsesRealLogsWithoutExceptions(self):
        linesParsed = 0
        expectedEntries = 591 + 84 + 768

        for i in xrange(3):
            filePath = os.path.join(
                    self.assetsPath, 'real{}.log'.format(i + 1))

            with open(filePath) as f:
                parser = rlp.RegexLogParser(self.cps[i], f)

                for entry in parser.parseLine():
                    linesParsed += 1
                    # test arbitrary field
                    self.assertIsNotNone(entry.getLogLevel())

        self.assertEqual(expectedEntries, linesParsed)

    def test_tokenizeConversionPattern(self):
        cps = [ '%D{%Y-%m-%d %H:%M:%S,%q} %-5p %x %t %c %l %m%n',
                '%D %x %t %n' ]

        cps_tokenized = [
                [ '%D{%Y-%m-%d %H:%M:%S,%q}',
                    '%-5p', '%x', '%t', '%c', '%l', '%m', '%n', ],
                [ '%D', '%x', '%t', '%n' ],
                ]

        for cp, t in zip(cps, cps_tokenized):
            tokenized = rlp.RegexLogParser._tokenizeConversionPattern(cp)
            self.assertEqual(len(tokenized), len(t))
            self.assertListEqual(t, tokenized)


if __name__ == '__main__':
    unittest.main()
