#!/usr/bin/python

import os
import StringIO
import logging
import unittest

from log2chart.parser.regex import propertyfile


class TestPropertyFile(unittest.TestCase):

    def setUp(self):
        logging.basicConfig(level = logging.ERROR)

        basePath = os.path.dirname(os.path.abspath(__file__))
        self.assetsPath = os.path.join(basePath, 'data')

        self.memFile = StringIO.StringIO()
        self.memFile.write("logger.settings.a=value\n")
        self.memFile.write("logger.settings.b=value2\n")

        self.propertyTree = { 'logger': {
            'settings': { 'a': 'value', 'b': 'value2'
                }
            }
            }


    def test_ifSeeksBackToTheBeginningOfTheFile(self):
        pf = propertyfile.PropertyFile(self.memFile)
        pfTree = pf.getPropertyTree()
        self.assertDictEqual(pfTree, self.propertyTree)


    def test_ignoresComments(self):
        self.memFile.write("# logger.settings.b=value2\n")
        self.memFile.write("#logger.settings.b=value2\n")
        self.memFile.write(" #logger.settings.b=value2\n")

        pf = propertyfile.PropertyFile(self.memFile)
        pfTree = pf.getPropertyTree()
        self.assertDictEqual(pfTree, self.propertyTree)


    def test_ifAppendsNonDictOptions(self):
        self.memFile.seek(0)
        self.memFile.write("logger.settings=value\n")
        self.memFile.write("logger.settings.b=value2\n")
        self.memFile.write("logger.settings.c=value3\n")

        self.propertyTree = { 'logger': {
            'settings': { 'settings': 'value',
                'b': 'value2',
                'c': 'value3',
                }
            }
            }

        pf = propertyfile.PropertyFile(self.memFile)
        pfTree = pf.getPropertyTree()
        self.assertDictEqual(pfTree, self.propertyTree)


    def test_ifIgnoresWhiteSpaceBetweenEqualSign(self):
        self.memFile.seek(0)
        self.memFile.write("logger.settings.a=  value\n")
        self.memFile.write("logger.settings.b  =value2\n")
        self.memFile.write("logger.settings.c = value3\n")

        self.propertyTree = { 'logger': {
            'settings': { 'a': 'value',
                'b': 'value2',
                'c': 'value3',
                }
            }
            }

        pf = propertyfile.PropertyFile(self.memFile)
        pfTree = pf.getPropertyTree()
        self.assertDictEqual(pfTree, self.propertyTree)


    def test_ifCorrectlyParsesExampleFile(self):
        filePath = os.path.join(self.assetsPath, 'log4cplus.props')
        propertyTree = {'log4cplus':
                {'appender':
                    {'STDERR': {
                        'STDERR': 'log4cplus::ConsoleAppender',
                        'layout': {'ConversionPattern': '%D{%Y-%m-%d %H:%M:%S,%q} %-5p %x %t %c %l %m%n',
                            'layout': 'log4cplus::PatternLayout'
                            },
                        'logToStdErr': 'true'
                        },
                    'bridgefile': {
                        'File': '/opt/zinc-trunk/var/log/dbusbridge.log',
                        'bridgefile': 'log4cplus::RollingFileAppender',
                        'layout': {'ConversionPattern': '%D{%Y-%m-%d %H:%M:%S,%q} %-5p %x %t %c %l %m%n',
                            'layout': 'log4cplus::PatternLayout'}}},
                        'logger': {'dbusbridge': 'INFO, STDERR, bridgefile'}
                    }
                }

        self.maxDiff = None

        with open(filePath) as f:
            pf = propertyfile.PropertyFile(f)
            pfTree = pf.getPropertyTree()

            self.assertDictEqual(pfTree, propertyTree)


if __name__ == '__main__':
    unittest.main()
