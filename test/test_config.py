#!/usr/bin/python

import logging
import os
import shutil
import tempfile
import unittest

from log2chart import config


class TestConfig(unittest.TestCase):

    def setUp(self):
        self.tempDir = tempfile.mkdtemp()
        self.confFile = tempfile.NamedTemporaryFile()

    def tearDown(self):
        shutil.rmtree(self.tempDir)

    def test_ifCreatesPath(self):
        path = os.path.join(self.tempDir, "some_dir", "file.conf")
        dirPath = os.path.dirname(path)
        c = config.Config(path)

        self.assertTrue(os.path.exists(dirPath))
        self.assertTrue(os.path.exists(path))

    def test_ifPreservesOldContent(self):
        entry = "a:b"
        self.confFile.write(entry)
        c = config.Config(self.confFile.name)
        self.confFile.seek(0)
        reread = self.confFile.readline()
        self.assertEqual(entry, reread)

    def test_ifParsesTheEntries(self):
        self.confFile.write("a:b\n")
        self.confFile.write("a2: c\n")
        self.confFile.write("a3 : d\n")
        self.confFile.write("a4 : e \n ")
        self.confFile.write(" a5 : f \n")
        self.confFile.flush()
        c = config.Config(self.confFile.name)
        self.assertEqual(c['a'], "b")
        self.assertEqual(c['a2'], "c")
        self.assertEqual(c['a3'], "d")
        self.assertEqual(c['a4'], "e")
        self.assertEqual(c['a5'], "f")

    def test_ifRaisesIfEntryIsAbsent(self):
        c = config.Config(self.confFile.name)
        with self.assertRaises(KeyError):
            x = c['abc']

    def test_ifFlushesContents(self):
        c = config.Config(self.confFile.name)

        c['a'] = 'b'
        c['c'] = 'd'
        c['number'] = '123'
        c.flush()

        c2 = config.Config(self.confFile.name)
        self.assertEqual(c['a'], c2['a'])
        self.assertEqual(c['c'], c2['c'])
        self.assertEqual(c['number'], c2['number'])


if __name__ == '__main__':
    unittest.main()

