#!/usr/bin/python

import logging
import unittest

from log2chart.parser.regex import fakepropertyfile


class TestFakePropertyFile(unittest.TestCase):

    def setUp(self):
        logging.basicConfig(level = logging.ERROR)
        self.fpf = fakepropertyfile.FakePropertyFile()


    def test_ifReturnsPropertyTree(self):
        propertyTree = {'log4cplus':
                { 'appender': { 'logfile': {
                    'layout': {
                        'ConversionPattern': '%D{%Y-%m-%d %H:%M:%S,%q} %-5p %x %t %c %l %m%n',
                        'layout': 'log4cplus::PatternLayout',
                        }
                    }
                    }
                    }
                }

        self.assertDictEqual(propertyTree,
                self.fpf.getPropertyTree())

if __name__ == '__main__':
    unittest.main()
