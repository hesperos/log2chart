#!/usr/bin/python

import unittest
import exceptions
import logging

from log2chart.parser.regex import patternconverterfactory as pcf


class TestPatternConverterFactory(unittest.TestCase):

    def setUp(self):
        logging.basicConfig(level = logging.ERROR)
        self.pcf = pcf.PatternConverterFactory()

    def test_ifRejectsNonPatterns(self):
        p1 = "doesn't start with %"
        with self.assertRaises(exceptions.RuntimeError):
            self.pcf.getPatternConverter(p1)

    def test_ifRaisesExceptionForNotSupportedPattern(self):
        p1 = "%Z"
        with self.assertRaises(exceptions.RuntimeError):
            self.pcf.getPatternConverter(p1)

    def test_buildsDateTimePattern(self):
        pass

if __name__ == '__main__':
    unittest.main()
