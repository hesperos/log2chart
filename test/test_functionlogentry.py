#!/usr/bin/python

import datetime
import exceptions
import functools
import logging
import unittest

from log2chart import functionlogentry, abstractlogentry

class TestFunctionLogEntry(unittest.TestCase):

    def setUp(self):
        logging.basicConfig(level = logging.ERROR)

    def test_ifIsLogEntry(self):
        e = functionlogentry.FunctionLogEntry(msg = "ENTER: abc")
        self.assertIsInstance(e, abstractlogentry.AbstractLogEntry)

    def test_ifRaisesIfNotAFunctionLogEntry(self):
        self.assertRaises(exceptions.RuntimeError,
                functionlogentry.FunctionLogEntry)

    def test_ifDiscardsInvalidTypes(self):
        c = functools.partial(
                functionlogentry.FunctionLogEntry, msg = "XXX: abc")
        self.assertRaises(exceptions.RuntimeError, c)

    def test_ifReturnsCorrectTypeAndSignature(self):
        entries = []
        entries.append(
                functionlogentry.FunctionLogEntry(msg = "ENTER: void abc()"))
        entries.append(
                functionlogentry.FunctionLogEntry(msg = "EXIT: int def()"))

        self.assertEqual(
                functionlogentry.FunctionLogEntry.ENTER, entries[0].getType())
        self.assertEqual(
                functionlogentry.FunctionLogEntry.EXIT, entries[1].getType())

        self.assertEqual("void abc()", entries[0].getSignature())
        self.assertEqual("int def()", entries[1].getSignature())

if __name__ == '__main__':
    unittest.main()
