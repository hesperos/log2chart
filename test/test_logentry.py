#!/usr/bin/python

import logging
import unittest
import datetime

from log2chart import logentry

class TestLogEntry(unittest.TestCase):

    def setUp(self):
        logging.basicConfig(level = logging.ERROR)

    def test_ifRaisesWithInvalidDate(self):
        with self.assertRaises(RuntimeError):
            logentry.LogEntry(threadid = '123')

        with self.assertRaises(RuntimeError):
            logentry.LogEntry(datetime = '123')

        with self.assertRaises(RuntimeError):
            logentry.LogEntry(location = ('xx', '123'))

    def test_ifReturnsConstructedData(self):
        dt = datetime.datetime.now()
        loglevel = 'DEBUG'
        logger = 'some_logger'
        threadid = 123456789
        process = 'my_daemon'
        sourcefile = 'main.cpp'
        linenumber = 123
        msg = 'example message'

        l = logentry.LogEntry(datetime = dt,
                loglevel = loglevel,
                logger = logger,
                process = process,
                threadid = threadid,
                location = (sourcefile, linenumber),
                msg = msg)

        self.assertEquals(dt, l.getDateTime())
        self.assertEquals(loglevel, l.getLogLevel())
        self.assertEquals(int(threadid), l.getThreadId())
        self.assertEquals(sourcefile, l.getSourceFile())
        self.assertEquals(linenumber, l.getLineNumber())
        self.assertEquals(msg, l.getLogMessage())
        self.assertEquals(logger, l.getLogger())
        self.assertEquals(process, l.getProcess())

    def test_ifPopulatesDefaultValues(self):
        l = logentry.LogEntry()

        self.assertEquals('INFO', l.getLogLevel())
        self.assertEquals(0, l.getThreadId())
        self.assertEquals('', l.getSourceFile())
        self.assertEquals(0, l.getLineNumber())
        self.assertEquals('', l.getLogMessage())


if __name__ == '__main__':
    unittest.main()
