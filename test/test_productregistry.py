#!/usr/bin/python

import unittest

from log2chart.productregistry import ProductRegistry, ProductRegistryMeta

class FakeRegistry(ProductRegistry):
    registry = {}
    __metaclass__ = ProductRegistryMeta

class DummyClass(object):
    pass

class TestDynamicLoader(unittest.TestCase):

    def test_ifRegistersClasses(self):
        r = FakeRegistry()
        r(DummyClass)
        self.assertEqual(1, len(FakeRegistry.registry))
        self.assertTrue(self.__module__ in FakeRegistry.registry)

    def test_ifRegistryIsClassProperty(self):
        r1 = FakeRegistry()
        r2 = FakeRegistry()

        r1(DummyClass)
        r2(DummyClass)

        self.assertEqual(r1.registry, r2.registry)

    def test_ifCanBeUsedAsDecorator(self):

        @FakeRegistry()
        class DummyDecorated(object):
            pass

        self.assertEqual(DummyDecorated, FakeRegistry.registry[self.__module__])

    def test_ifThrowsIfRegistryDoesntContainRegistry(self):

        with self.assertRaises(TypeError):
            class IncorrectRegistry(ProductRegistry):
                __metaclass__ = ProductRegistryMeta


if __name__ == '__main__':
    unittest.main()


