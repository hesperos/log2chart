#!/usr/bin/python

import unittest
import __builtin__
import functools

from log2chart.registeredobjectproxy import RegisteredObjectProxy

class FakeImport(object):
    def __init__(self):
        self.args = []
        self.kwargs = {}
        self.counter = 0

    def fakeImport(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs
        self.counter += 1

class FakeRegistry(object):
    registry = {}

    def addModule(self, module, cls):
        FakeRegistry.registry[module] = cls


class TestRegisteredObjectProxy(unittest.TestCase):
    def setUp(self):
        self.fakeImport = FakeImport()
        self.originalImport = __builtin__.__import__
        __builtin__.__import__ = functools.partial(self.fakeImport.fakeImport,
                self.fakeImport)

        self.FakeRegistry = FakeRegistry()

    def tearDown(self):
        __builtin__.__import__ = self.originalImport

    def test_ifAttemptsToLoadSymbolsOfGivenModule(self):
        someArgs = 1, 'test', 45.0
        moduleName = 'someModule'

        class DummyClass(object):
            def __init__(self, *args):
                self.args = args

        self.FakeRegistry.addModule(moduleName, DummyClass)
        rop = RegisteredObjectProxy(FakeRegistry, moduleName, *someArgs)

        self.assertEqual(self.fakeImport.counter, 1)
        self.assertEqual(rop.object.args, someArgs)

    def test_ifForwardsAttributeReferences(self):
        someArgs = 1, 'test', 45.0
        moduleName = 'someModule'

        class DummyClass(object):
            def __init__(self, *args):
                self.args = args

            def funcA(self):
                self.funcA = 1

            def funcB(self, arg):
                self.funcB = arg

        self.FakeRegistry.addModule(moduleName, DummyClass)
        rop = RegisteredObjectProxy(FakeRegistry, moduleName, *someArgs)

        funcArg = 'someArgument'

        try:
            rop.funcA()
            rop.funcB(funcArg)
        except:
            self.fail("Shouldn't raise")

        self.assertEqual(rop.object.args, someArgs)
        self.assertEqual(rop.object.funcA, 1)
        self.assertEqual(rop.object.funcB, funcArg)

