#!/usr/bin/python

import unittest
import logging

from log2chart.logger import Logger


class TestLogger(unittest.TestCase):

    def setUp(self):
        self.originalLogLevel = Logger().get().getEffectiveLevel()

    def tearDown(self):
        Logger()._setLevel(self.originalLogLevel)

    def test_ifVerbosityToLevelReturnsExpectedValuesForIncorrectLevels(self):
        self.assertEqual(logging.DEBUG, Logger._verbosityToLevel(123))
        self.assertEqual(logging.DEBUG, Logger._verbosityToLevel(-10))
        self.assertEqual(logging.DEBUG, Logger._verbosityToLevel(-1))

    def test_ifVerbosityToLevelReturnsExpectedValuesForCorrectLevels(self):
        self.assertEqual(logging.ERROR, Logger._verbosityToLevel(0))
        self.assertEqual(logging.WARNING, Logger._verbosityToLevel(1))
        self.assertEqual(logging.INFO, Logger._verbosityToLevel(2))
        self.assertEqual(logging.DEBUG, Logger._verbosityToLevel(3))
        self.assertEqual(logging.DEBUG, Logger._verbosityToLevel(4))
        self.assertEqual(logging.DEBUG, Logger._verbosityToLevel(5))

    def test_ifReturnedLoggerIsNotNone(self):
        self.assertNotEqual(None, Logger().get())

    def test_ifSetVerbositySetsLevelsAccoringly(self):
        logger = Logger()

        for v in xrange(100):
            expected = Logger._verbosityToLevel(v)
            logger.setVerbosity(v)
            self.assertEqual(expected, logger.get().getEffectiveLevel())



if __name__ == '__main__':
    unittest.main()



