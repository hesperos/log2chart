# log2chart

[![Build Status](https://travis-ci.org/dagon666/log2chart.svg?branch=master)](https://travis-ci.org/dagon666/log2chart)

## Summary

This small tool is a simple attempt to parse log2j log files in order to get
some more data out of them. It's a modular compact solution using plug-in based
framework which makes it easy extendible as well.

## Installation

    python setup.py install

Alternatively use virtualenv prior to installation.

## Usage

    log2chart --property-file ~/properties.props -a modulefile barh ~/logfile.log
