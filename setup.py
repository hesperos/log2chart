#!/usr/bin/python

from setuptools import setup, find_packages

setup(name = 'log2chart',
    version = '1.0',
    packages = find_packages(),

    install_requires = [
        ],

    extras_require = {
        'barh': [ 'matplotlib>=1.0.0', ],
        },

    entry_points = {
        'console_scripts': [
            'log2chart = log2chart.main:main',
            ],
        },

    author = "Tomasz Wisniewski",
    author_email = "tomasz.wisni3wski@gmail.com",
    description = "simple log2j log parser",
    url = "",
)
